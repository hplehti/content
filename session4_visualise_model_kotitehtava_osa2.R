#' ---
#' title: "Kotitehtävä 4 - osa2"
#' author: hpleht@utu.fi
#' output:
#'   html_document:
#'  #   toc: true
#'  #   toc_float: true
#'     number_sections: yes
#'     code_folding: show
#' ---

#' [Linkki kotitehtävän lähdekoodiin gitlab:ssa](https://gitlab.com/utur2016/content/raw/master/session4_visualise_model_kotitehtava.R)


#+ setup, include=FALSE
library(knitr)
opts_chunk$set(list(echo=TRUE,eval=FALSE,cache=FALSE,warning=TRUE,message=TRUE))
library(tidyverse)

#' # Lyhyt johdanto grafiikkaformaatteihin
#' 
#' Tilastografiikat tekemisessä on tärkeää tietää perusasiat eri grafiikkaformaateista. Grafiikkaformaattien kaksi päälinjaa
#' ovat [bittikarttagrafiikka](https://fi.wikipedia.org/wiki/Bittikarttagrafiikka) ja 
#' [vektorigrafiikka](https://fi.wikipedia.org/wiki/Vektorigrafiikka), joiden ominaisuudet tiivistetty ao. taulukkoon. 
#' Lue myös yo. lyhyet wikipedia-artikkelit mikäli ero ei ole aivan selvä.
#' 
#' 
#' ## Vektorigrafiikka vs. bittimappigrafiikka
#' 
#' |                         |    bittikartta (bitmap)           |                       vektori                      |
#' |------------------------ |-----------------------------------|----------------------------------------------------|
#' | tiedostopääte       | .jpg, .png, .gif                  | .eps, .pdf, .svg, .ai                              |
#' | esimerkiksi         | digikuva                          | googlen kartat                                     |
#' | koostuu           | miljoonista pikseleistä                 | pisteistä, viivoista ja polygoneista         |
#' | tiedostokoko        | suuri                             | pieni                                              |
#' | muokkausohjelmisto  | [Gimp](www.gimp.org/) (Photoshop) | [Inkscape](https://inkscape.org/en/) (Illustrator) |
#' | sopii               | verkoon, printtiin (korkearesoluutioisena)       | printtiin, jälkikäsittelyyn, -svg-muodossa myös verkkoon |
#' 
#'  **Vektorigrafiikasta voi tehdä bittimappigrafiikkaa, mutta ei toisin päin**
#'
#' R:ssä tehtyjä kuvia voit tallentaa useisiin erilaisiin sekä vektori- että bittikarttaformaattiehin.
#'  
#'   
#'     
#' ## Staattinen vs. vuorovaikutteinen grafiikka
#' 
#' Bittikartta- ja vektorigrafiikka ovat ensisijaisesti ns. staattista grafiikka, jossa ei ole vuorovaikutteisia 
#' ominaisuuksia. "Tuktkimusviestinnässä" perinteisen printattavan tutkimusraportin ohella yleistyy erilaiset verkkototeutukset 
#' kuten blogit tai verkkosovellukset. Kun sisältö on palvelimella ja lukeminen tapahtuu selaimella, niin kaikenlaiset verkkoteknologiat
#' ovat käytettävissä. Verkkototeutusten kaksi päälinjaa, vuorovaikutteinen grafiikka & verkkosovellukset, on vedetty yhteen alla olevaan
#' taulukkoon. 
#' 
#' 
#' |  grafiikan tyyppi      |            pros            |        cons        |
#' | ---------------------- | -------------------------- | ------------------ |
#' | interaktiiviset kuviot | teknologian kehitys nopeaa | lyhyt elinkaari päivittyvien riippuvuuksien myötä  |
#' |                        | paljon vaihtoehtoja  | hyvin sekava skene  |
#' |                        |                            |  tarvitsee verkkoyhteydet                  |
#' |                        |                            |  ei voi printata!                  |
#' | verkkosovellukset | hyvin joustavia              | hostaus ja ylläpito      |
#' |                   | pystyy kaikkeen mihin R & linux  | vaatii paljon räätälöintia |
#' 
#' R:ssä näiden kanssa pääset alkuun seuraavista linkeistä:
#' 
#' - [htmlwidgets for R](http://www.htmlwidgets.org/) - paras paikka aloittaa vuorovaikutteisen grafiikan kokeilut
#' - [Shiny - A web application framework for R](http://shiny.rstudio.com/) - täältä alkuun verkkosovellusten teossa 
#' 
#' # Osio1: Kuvien tallentaminen levylle
#' 
#' Rstudiossa voit valita **Plots**-paneelista *Export* ja tallentaa kuvan *Save as image* 
#' dialogista kuuteen eri formaattiin:
#' 
#' - [PNG](https://en.wikipedia.org/wiki/Portable_Network_Graphics) - bittikartta-formaatti - tukee läpinäkyvyyttä - varma valinta verkkoon
#' - [JPEG/JPG)](https://en.wikipedia.org/wiki/JPEG) - bittikartta-formaatti - pakattavissa pieneen tilaan - varma valinta verkkoon
#' - [TIFF](https://en.wikipedia.org/wiki/TIFF) - bittikartta-formaatti - pakkaamaton, korkearesoluutioisena printtiin
#' - [BMP](https://en.wikipedia.org/wiki/BMP_file_format) - bittikartta-formaatti - perinteinen pakkaamaton formaatti. katoamassa.
#' - [SVG](https://en.wikipedia.org/wiki/Scalable_Vector_Graphics) - *scalable vector graphics* - avoin vektorigrafiikkamuoto. Uudet selaimet tukevat. Inkscape-grafiikkaohjelman oletusmuoto
#' - [EPS](https://en.wikipedia.org/wiki/Encapsulated_PostScript) - *Encapsulated PostScript* - 
#' 
#'  sekä pdf-formaattiin kohdasta "Save as PDF".
#'  
#'  pdf-formaatti on nykyisellään parhaiten yhteensopiva vektorigrafiikan muoto. Suurin haaste on [eri fonttien käyttö](http://blog.revolutionanalytics.com/2012/09/how-to-use-your-favorite-fonts-in-r-charts.html)
#' 
#' 
#' ## ggplot2-kuvien tallentaminen eri formaatteihin
#' 
#' ggplot2-paketilla tehtyjä kuvia voi tallentaa Rstudio-käyttöliittymästä käsin samoin kuin kaikkia muitakin formaatteja. 
#' Tallentaminen on kuitenkin näppärämpää kirjoittaa kuvion koodin yhteyteen ´ggsave()`-funktiota käyttäen. ´ggsave()`-funktio 
#' ymmärtää kuvion nimestä, mihin formaattiin kuva tallennetaan. Jos teet kuvion alla olevalla koodilla
#' 
#' `kuva <- ggplot(data=cars, aes(x=speed,y=dist)) + geom_point()`
#' 
#' Voit tallentaa sen eri formaatteihin koodilla
#' 
#' - `ggsave(plot=kuva, filename = "kuva.png")` - bittimappi png
#' - `ggsave(plot=kuva, filename = "kuva.pdf")` - vektori pdf
#' - `ggsave(plot=kuva, filename = "kuva.svg")` - vektori svg
#' 
#' Lisäparametreinä ´ggsave()`-funktioon voi laittaa  mm. seuraavat
#' 
#' - `filename`	File name to create on disk.
#' - `plot`	Plot to save, defaults to last plot displayed.
#' - `device` Device to use (function or any of the recognized extensions, e.g. "pdf"). By default, extracted from filename extension. ggsave currently recognises eps/ps, tex (pictex), pdf, jpeg, tiff, png, bmp, svg and wmf (windows only).
#' - `path`	Path to save plot to (combined with filename).
#' - `scale`	 Multiplicative scaling factor.
#' - `width`, `height`	 Plot dimensions, defaults to size of current graphics device.
#' - `units`	 Units for width and height when specified explicitly (in, cm, or mm)
#' - `dpi`	Resolution used for raster outputs.
#' - `limitsize	When TRUE (the default), ggsave will not save images larger than 50x50 inches, to prevent the common error of specifying dimensions in pixels.
#' 
#' 
#' **Mikäli et ole vielä luonut kansiota `kotitehtava4/kuviot` luo se komennolla
#' `dir.create(path="./kotitehtava4/kuviot", recursive=TRUE, showWarnings = FALSE)`**
#' 
#' **Piirrä kuvat koodilla kuva <- ggplot(data=cars, aes(x=speed,y=dist)) + geom_point()`
#' ja kirjoita miten se tallennetaan ko. kansioon png, svg, tiff, jpg, eps, pdf ja bmp muodoissa.**
#' 
#' **Avaan kaikki kuvat koneellasi ja tarkastele niitä!**
#' 
#+ vastaus1

kuva <- ggplot(data=cars, aes(x=speed,y=dist)) + geom_point() 
kuva 

ggsave(plot=kuva, filename = "kotitehtävät4/kuviot/kuva.png")
ggsave(plot=kuva, filename = "kotitehtävät4/kuviot/kuva.svg")
ggsave(plot=kuva, filename = "kotitehtävät4/kuviot/kuva.tiff")
ggsave(plot=kuva, filename = "kotitehtävät4/kuviot/kuva.jpg")
ggsave(plot=kuva, filename = "kotitehtävät4/kuviot/kuva.eps")
ggsave(plot=kuva, filename = "kotitehtävät4/kuviot/kuva.pdf")
ggsave(plot=kuva, filename = "kotitehtävät4/kuviot/kuva.bmp")



#' # Jakauminen piirtäminen
#' 
#' Jatketaan malesdatan  kanssa jonka voit siis ladata komennolla
#' `malesdata <- readRDS(gzcon(url("http://courses.markuskainu.fi/utur2016/database/malesdata.RDS")))`
#' 
#' Tee datan muuttujasta `wage` sekä histogrammi, tiheyskuvio (density plot) sekä laatikkojanakuvio ja tallenna ne
#' .pdf-muotoon koossa leveys = 6, korkeus= 5, yo. nimillä `kotitehtava4/kuviot`-kansioon
#' 
#+vastaus2
malesdata <- readRDS(gzcon(url("http://courses.markuskainu.fi/utur2016/database/malesdata.RDS")))

histo <- ggplot(malesdata, aes(x=wage)) + geom_histogram()
histo

dens <- ggplot(malesdata, aes(x=wage)) + geom_density()
dens

box <- ggplot(malesdata, aes(x=factor(0),wage)) + geom_boxplot()
box

ggsave(plot=box, filename ="kotitehtävät4/kuviot/box.pdf", width=6, height=5)
ggsave(plot=histo, filename = "kotitehtävät4/kuviot/histo.pdf", width = 6, height = 5)
ggsave(plot=dens, filename = "kotitehtävät4/kuviot/dens.pdf", , width = 6, height = 5,)


#' Katso geom_histogram() dokumentaatiota ja etsi sieltä kuva, jossa on ns. pinottu histogrammi. 
#' Siis pylväissä on päällekkäin eri luokittelevan muuttujan tapaukset.
#' 
#' Tee vastaava histogrammi wage muuttujasta niin että ethn-muuttujan luokat ovat palkkeina

hist2<-ggplot(malesdata,aes(wage, fill=ethn)) + geom_histogram()
hist2


#' # Vaihtoehtoiset koordinaattijärjestelmät
#' 
#' Piirakkakuvio on datavisualisointien klassikko, joka eroaa aikasemmista juuri koordinaattijärjestelmän
#' osalta. Grammar of graphics -ajattelussa piirakkakuvio on päällekkäin pinottu (stacked) histogrammi,
#' jossa on polaarinen koordinaattijärjestelmä.
#' 
#' Tutustu coord_polar()-dokumentaatioon: <http://docs.ggplot2.org/current/coord_polar.html> ja 
#' tee malesdata:sta piirakkakuvio, jossa väreinä on ethn-muuttujan luokat.


pie <- ggplot(malesdata, aes(x = factor(ethn), fill=ethn)) + geom_bar(width = 1)
pie + coord_polar()



#' # Faktoreiden järjesteleminen 
#' 
#' Tee malesdata-muuttujasta yhteenvetotaulukko `dplyr::summarise-funktiolla objektiksi `dat`, jossa 
#' on eri etnh-muuttujan luokkien mediaanipalkat. Tee `dat`-datan ethn-muuttujasta faktori ja
#' ja järjestä luokat käänteiseen aakkosjärjestykseen, ja piirra siitä tolppakuvio, jossa 
#' x-akselilla ethn-muuttuja ja y-akselilla mediaanipalkka.


library(dplyr)
library(forcats)
dat <- malesdata %>% group_by(ethn) %>%  summarise(med = median(wage))

dat$ethn <- factor(dat$ethn)
dat$ethn <- fct_rev(dat$ethn)
bar1 <- ggplot(dat, aes(x=ethn, y=med, fill=ethn)) + geom_bar(stat="identity") 
bar1


  #' Tutustu forcats-paketin dokumentaatioon: <http://forcats.tidyverse.org/> ja tee vastaava
#' yhteenvetotaulukko malesdatasta industry-muuttujan suhteen.
#' 
#' Järjestä yhteenvetotaulukon teollisuudenala-muuttujan luokat keskipalkan mukaan faktorin leveleiksi
#' (`fct_reorder()`) ja pirrä vastaava tolppakuvio kuin edellä.
#' 


ind <- malesdata %>% group_by(industry) %>%  summarise(med_ind = median(wage))

ind$industry <- factor(ind$industry)

ind$industry <- fct_reorder(ind$industry, ind$med_ind)

bar2 <- ggplot(ind, aes(x=industry, y=med_ind)) + geom_bar(stat="identity") + theme(axis.text.x  = element_text(angle=90))
bar2

#' # Jokeri1
#' Alkuperäiset kuvat: <http://koti.kapsi.fi/~muuankarski/fpa/elinolot2016/johdanto_kuvat.html>
#+jokeri1, eval=FALSE

#' Tee vastaava kuvio jostain sinua kiinnostavasta Eurostatin muuttujaparista, muokkaa maaryhmittelyä 
#' ja värejä. Asenna ensin tarvittavat paketit, mikäli ne puuttuvat! Tallenna se nimellä
#' `kotitehtava4/kuviot/`-kansioon .pdf-muodossa koossa joka näyttää pdf-lukijassa hyvältä


install.packages("eurostat")
install.packages("countrycode")

library(tidyverse)                      
library(eurostat)
library(countrycode)

# maahanmuuttajat
# search_eurostat("asylum seekers")
d <- get_eurostat(id = "tps00191", time_format = "raw")
# lab <- label_eurostat(d)

d %>% select(geo,time,values) -> mig

# BKT per kapita 
#search_eurostat("GDP per capita")
d <- get_eurostat(id = "tec00114", time_format = "raw")
# lab <- label_eurostat(d)

d %>%  select(geo,time,values) -> gdp

mig %>% 
  left_join(gdp, by = c("geo","time")) -> df.scatter

df.scatter <- merge(mig,gdp,by=c("geo","time"))
names(df.scatter) <- c("geo.time","year","mig","gdp")
#

point_palette <- c("#999999", "#E69F00", "#56B4E9", "#009E73","#D55E00", "#CC79A7","#0072B2","#F0E442")


df.scatter$geo.time <- as.character(df.scatter$geo.time)
df.scatter$geo.time[df.scatter$geo.time == "UK"] <- "GB"
df.scatter$maa <- countrycode(df.scatter$geo.time, "iso2c", "country.name")

# regimes
is(df.scatter$maa)
df.scatter$regime[df.scatter$maa %in% c("Austria", "Belgium", "Switzerland", "Germany", "France", "Luxembourg", "Netherlands")] <- "Western Central Europe"
df.scatter$regime[df.scatter$maa %in% c("Bulgaria", "Czech Republic", "Estonia", "Hungary", "Lithuania", "Latvia", "Poland", "Romania", "Slovenia", "Slovak Republic", "Croatia","Slovakia")] <- "Eastern Europe"
df.scatter$regime[df.scatter$maa %in% c("Cyprus", "Greece", "Spain", "Italy", "Malta", "Portugal")] <- "Southern Europe"
df.scatter$regime[df.scatter$maa %in% c("Denmark", "Finland", "Iceland", "Norway", "Sweden")] <- "Nordic Countries"
df.scatter$regime[df.scatter$maa %in% c("Ireland", "United Kingdom","Australia","United States","Canada","New Zealand")] <- "Anglo-Saxon Countries"
df.scatter$regime[df.scatter$maa %in% c("Japan", "Korea, Republic of","Korea")] <- "East Asian"
df.scatter$regime[!(df.scatter$regime %in% c("Western Central Europe",
                 "Eastern Europe",
                 "Southern Europe",
                 "Nordic Countries",
                 "Anglo-Saxon Countries",
                 "East Asian"))] <- NA


scatterplot <-  ggplot(data=df.scatter %>% filter(year == 2014) %>% 
                         na.omit(), 
                       aes(x=gdp,y=mig)) +
  geom_point(aes(color=regime), size=3) +
  geom_label(aes(fill=regime,label=maa),
             color="white",size=4, nudge_y=-0.5, nudge_x=-0.2, show.legend = F) +
  labs(y="Asylum seekers", x="GDP per capita") +
  # coord_cartesian(xlim=c(5,25),ylim=c(12,35)) +
  scale_color_manual(values=point_palette) +
  scale_fill_manual(values=point_palette) +
  theme_light() +
  theme(legend.title=element_blank()) +
  theme(legend.text=element_text(size=12)) +
  theme(legend.position="top") +
  theme(text=element_text(family="Open Sans")) +
  #geom_smooth(method=lm, se=FALSE) +
  theme(axis.title.y = element_text(size=12, face="bold")) +
  theme(axis.title.x = element_text(size=12, face="bold")) +
  theme(axis.text.y = element_text(size=10)) +
  theme(axis.text.x = element_text(size=10)) +
  guides(color = guide_legend(nrow = 1)) +
  theme(legend.key.size = unit(6, "mm")) +
  labs(title="Asylum seekers and GDP per capita")
scatterplot
#' 




#' # Jokeri2
#' 
#' Tein demon karttojen tekemisestä osaksi kirjoittamani `eurostat`-paketin 
#' avulla tönne: <https://rpubs.com/muuankarski/210495>. 
#' 
#' Tutustu myös viralliseen 
#' vignetteen: https://github.com/rOpenGov/eurostat/blob/master/vignettes/eurostat_tutorial.md ja 
#' blogipostaukseen: http://ropengov.github.io/r/2015/05/01/eurostat-package-examples/. 
#' 
#' **Tee itseäsi kiinnostavasta indikaattorista vastaava kartta ja tallenna se nimellä
#' `kotitehtava4/kuviot/`-kansioon .pdf-muodossa koossa joka näyttää pdf-lukijassa hyvältä**
#' 
#+vastaus8


library(eurostat)
library(dplyr)
library(ggplot2)
install.packages("stringr")
library(stringr)
install.packages("mapproj")
libraty(mapproj)

library(eurostat)
library(dplyr)
library(ggplot2)
library(stringr)
unemp <- search_eurostat("unemployment")

# Data from Eurostat
get_eurostat("tps00191", time_format = "raw", stringsAsFactors = FALSE)  %>%
  filter(sex %in% "T",
         time == 2015) %>%
  # we need a country var for reproducing the Eurostat map
  # # categorise
  mutate(cat = cut_to_classes(values, n=7, decimals = 1)) %>%
  # merge with geodata
  merge_eurostat_geodata(data=.,geocolumn="geo",resolution = "20", output_class = "df", all_regions = FALSE) %>%
  # plot map
  ggplot(data=., aes(x=long,y=lat,group=group)) +
  geom_polygon(aes(fill=cat),color="dim grey", size=.1) +
  scale_fill_manual(values = c("#d73027","#fc8d59","#fee08b","#ffffbf","#d9ef8b","#91cf60","#1a9850")) +
  # scale_fill_continuous(trans = 'reverse', ) +
  guides(fill = guide_legend(reverse=T, title = "")) +
  labs(title="Asylum seekers",
       subtitle="%",
       caption="(C) EuroGeographics for the administrative boundaries 
       Map produced in R with a help from Eurostat-package <github.com/ropengov/eurostat/>") +
  theme_light() + theme(legend.position=c(.8,.8), text=element_text(family="opensans")) +
  coord_map(project="orthographic", xlim=c(-12,44), ylim=c(35,70))






library(eurostat)
library(dplyr)
library(ggplot2)
library(stringr)
unemp <- search_eurostat("unemployment")

# Data from Eurostat
get_eurostat("lfst_r_lfe2emprt", time_format = "raw", stringsAsFactors = FALSE)  %>%
  filter(sex %in% "T",
         nchar(geo) == 4,
         time == 2015) -> d
  # we need a country var for reproducing the Eurostat map
  # # categorise
  dplyr::mutate(cat = cut_to_classes(values, n=7, decimals = 1)) %>%
  # merge with geodata
  merge_eurostat_geodata(data=.,geocolumn="geo",resolution = "20", output_class = "df", all_regions = FALSE) %>% 
  # plot map
  ggplot(data=., aes(x=long,y=lat,group=group)) +
  geom_polygon(aes(fill=cat),color="dim grey", size=.1) +
  scale_fill_manual(values = c("#d73027","#fc8d59","#fee08b","#ffffbf","#d9ef8b","#91cf60","#1a9850")) +
  # scale_fill_continuous(trans = 'reverse', ) +
  guides(fill = guide_legend(reverse=T, title = "Employment rate (%)")) +
  labs(title="Employment rate, persons aged 20-64, by NUTS 2 regions, 2015",
       subtitle="%",
       caption="(C) EuroGeographics for the administrative boundaries 
       Map produced in R with a help from Eurostat-package <github.com/ropengov/eurostat/>") +
  theme_light() + theme(legend.position=c(.8,.8), text=element_text(family="opensans")) +
  coord_map(project="orthographic", xlim=c(-12,44), ylim=c(35,70))
 